<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
 
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_sc');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'yyicrgi7pjizjhiojtaxmxpg6anyobr1xmakayr9svifrdsjsusgdnpskigqqi8m');
define('SECURE_AUTH_KEY',  'o1shhznkkj0gkxzorvzsxyjr4h0kubqlyecugtezbd1mbnhnyvt5wzzo5fbcbihi');
define('LOGGED_IN_KEY',    'lwairpcf5v8p7geqsy5kz3ofr2mxv2es4c6p8an2l6cjkremzd8wd5i6a0wypxix');
define('NONCE_KEY',        'fpf8kyzec2kpvwbf0cocz4dywoy1n5dmtodleuybagn0tu6dyitwc5eftfydyblm');
define('AUTH_SALT',        'o59xxldrx62z6txxby7zxrmakuyoacg2iml7yvygge8kv8jfz2zn5ztzsuv6ffmf');
define('SECURE_AUTH_SALT', '9ranwv2yt00qq5eluozjv3gpcjiuuyelcsugu2avfe0efontrwkfqc9mu8j9jmuz');
define('LOGGED_IN_SALT',   '2vlxcaif0py9uxuqnnmkp98wws4har6pktuixw4atpfpoxtdy7nwbu4m0zfpyvvi');
define('NONCE_SALT',       '4ffdpj3b2lbcrperh7q17gzqqp0bjur15v7jierj5eqlaavgrdh9g10xqugfvuny');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'scwp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
