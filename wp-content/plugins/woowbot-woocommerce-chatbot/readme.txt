=== ChatBot for WooCommerce WoowBot ===
Contributors: quantumcloud
Donate link: http://www.quantumcloud.com
Tags: bot, chatbot, live chat, chat bot, faceBook messenger, woocommerce, woocommerce chatbot, conversational commerce, shopbot
Requires at least: 4.0
Tested up to: 4.9
Stable tag: 1.9.0
Requires PHP: 5.4.0
License: GPLv2 or later
License URI: http://www.quantumcloud.com

WoowBot is a Chat Bot for WooCommerce. This stand alone, no facebook required chatbot or shopbot helps shoppers find products easily and increase sales! 

== Description ==
= What is WoowBot WooCommerce ChatBot? =
WoowBot is a stand alone WooCommerce ChatBot with zero configuration or bot training required. This plug and play chat bot or conversational commerce shopbot also does not require any 3rd party service integration like Facebook though Facebook Messenger Live Chat is integrated with the Pro version of this plugin as additional feature. This shopping bot helps shoppers find the products they are looking for easily and increase store sales through conversational chat and increase your customer conversion rate!

You can make this plug n’ play Chat Bot to work with any language with very little effort.

= Next Generation Conversational Commerce for WooCommerce =
Conversational commerce is the future of eCommerce. Enter WoowBot: a new way of customer interaction that allows brands to connect with their customers on a personal level with shopbots while creating a more natural interaction that mimics the physical world. 24/7/365 WoowBot ChatBot can provide live chat support to your shoppers and provide product search services, order status (pro feature) or support(pro feature). Increase customer satisfaction, reduce costs to scale and improve ability to engage customers. 

<strong>WoowBot Chat Bot Pro version also comes with the possibility of creating mobile app!</strong>

> [View Demo of Chat Bot WoowBot PRO Version](http://dev.quantumcloud.com/woowbot/)
> <strong>For the Advanced Features Upgrade to [WoowBot PRO](https://www.quantumcloud.com/products/woocommerce-chatbot-woowbot/)</strong> 

Getting Started with the Chat Bot WoowBot Pro
[youtube https://www.youtube.com/watch?v=Ho1EKixM-V8]

= What Can this Chat Bot do? =
WooWBot is first of its kind chatbot shop assistant for <strong>WooCommerce</strong>. It is a Plug n’ play, Stand Alone WooCommerce Shopping Chat Bot that can help Increase your store Sales. Shoppers can converse fluidly with the Bot – thanks to the Integration with Google‘s Artificial Intelligence and Natural Language Processing (AI and NLP), Search and Add products to the cart directly from the chat interface and get Support.

When activated, WooWBot Chat Bot sits there *floating* at the right bottom corner of website and offers users to find the right product through intelligent chat conversation based search like a live assistant. WoowBot can also provide support and allow shoppers to contact the store admin from the chat in various ways.


= WoowBot Free vs Pro Version =

WoowBot free version is a great Chat Bot tool for a small online store that does not require the advanced features of the pro version bot. The free version features list:

* One template
* Chat and search for products
* Search using WooCommerce standard search function that uses title and description
* Display products found and link to the product page
* Adjust location of the button
* Upload Custom Chat Icon
* Language center to modify bot responses (in any language)
* Custom CSS box
* MO/Port included for translation of multi language in back end

= WoowBot Pro Feature Highlights =

<strong>Artificial Intelligence and NLP </strong>

WoowBot Pro is Intelligent. Integrated with Google’s Natural Language Processing and AI through DialogFlow. It is capable of Small Talk, can carry on a conversation flow and fulfill any reasonable request. Required training data is bundled with the rpo version of the plugin.

How to add artificial intelligence natural language processing (NLP) to WoowBot
[youtube https://www.youtube.com/watch?v=Hxog956CFWk]

<strong>Onsite Retargeting, Exit Intent</strong>

Recover up to 25% of Abandoning Visitors with Onsite Retargeting. WoowBot detects exiting Visitors and displays a targeted offer to your visitor determined by you. Up to 25% of retargeted visitors will respond to your message and turn into customers. Targeted offers can be a coupon code, free ebook etc.
In-Chat Support and Contact

The Onsite Retargeting helps your Conversion rate optimization by showing special offers and coupons on Exit Intent, time interval or page scroll-down inside the ChatBot window. Track Customer Conversions with statistics to find out if shoppers are abandoning carts without completing orders. Get more sales!

Onsite retargeting with Chatbot WoowBot
[youtube https://www.youtube.com/watch?v=8JVq08-bGic]

<strong>Reduce Shopping Cart Abandonment</strong>
In addition to exit intent and remarketing offers, WoowBot can remind users to complete the checkout process when shoppers have products in the cart but they are not completing the order for some reason. Many shoppers add products to the cart but leave the site from other pages, after continuing to browse, before or after they reached the cart page. 

If a user leaves the site and later comes back – WoowBot will remind the shopper with a custom message to complete the order!

All of these will translate into a much higher rate of customer conversion by reducing the potentials for Abandoned Cart.

<strong>In-Chat Support and Contact</strong>

WoowBot pro provides support directly from the Chat window. You can add commonly asked questions (FAQ) and answers in the backend that WoowBot will display when user goes to support area. If the shopper does not find answers to his question in the FAQ, he can opt to send a email to site admin or leave feedback!

<strong>Schedule the ChatBot to Work with other Live Chat Service</strong>

You can schedule WoowBot Pro to run only when you are not available for Live chat with other services – outside of your normal office hours. The Onsite Retargeting helps your Conversion rate optimization by showing your special offers and coupons on Exit Intent, time interval or page scroll down inside the chatbot window. Get rid of those other Exit Intent popups with a chatbot that talks directly with the shopper. Reduce Abandoned Cart by showing timely messages with the Chat Bot!

Here is a list of WoowBot pro version features:

* Advanced, fast search with database indexing
* Google Artificial Intelligence or AI Engine
* Natural Language Processing through Google’s Dialog Flow
* Search additional WooCommerce product fields like category name, tags, excerpt, SKU etc.
* Show or hide cart item number
* Fine tune WoowBot icon position
* Product display order by and sorting options
* Option to Show Only Parent Categories with or without Sub Category list.
* Option to display order status with or without logging in
* Option to choose on which pages WoowBot should load
* Upload custom ChatBot icon
* Upload custom Agent icon
* Choose from 4 design templates for ChatBot interface
* Option to disable WooWBot on Mobile Devices
* Option to exclude out of stock products from search results
* Option to Enable/Disable Product Search, Featured Products, Sale Products, Order Status buttons at start
* Show or Hide Opening Notifications
* Upload your own background image for chatbot
* Create FAQ area with multiple questions and answers (supports html)
* Add video in Support area just by pasting Youtube link
* Add multiple store notifications to show above the ChatBot icon
* Show recently viewed products for easy reference to the shopper
* Show featured products until shopper has viewed products
* Quick Cart view
* Quick access to Support
* Quick Help for commands that can be used in-chat
* Admin customizable chat commands
* Stop Words dictionary included and editable by admin. Bot will automatically exclude stop words from search criteria and chat commands
* Advanced Language Center to edit and change every WoowBot responses, System languages, stop words and info messages!
* Add multiple variations of ChatBot responses for each node. They will be used randomly and give an appearance of more human like responses.
* Display product details in-chat - complete with images, add to cart option and support for multiple images
* Option to open product details in page instead of Bot window
* Persistent chat history over shopper session on website
* Remember chat history in browser local storage and greet returning shoppers
* Shortcode for WoowBot on Page
* Multi Language support. mo/pot file included so you can translate to any language
* RTL support
* Integration with FaceBook Messenger for Live Chat
* Integration with Skype, WhatsApp, Viber, Web Link & Phone Call
* Call me back – customer leaves phone number.
* Collect Customer Feedback by email option.
* Show customer retargeting messages  on Exit Intent, After Scrolling Down “X” Percent, Or after “X” seconds.
* Custom Background color for retargeting messages.
* Checkout reminder on set time interval to reduce cart abandonment.
* Checkout reminder when shopper comes back to the site and has products on the cart - reduce abadoned cart.
* Schedule day and time when WoowBot will run. Make WoowBot work with other Live chat software.
* SCustomer Conversion Reporting with Charts and Graphs
* SShopper Conversion Statistics by Day, Week, Month and Custom Date Range
* Enable chat bot as mobile app feature

= WoowBot Mobile App =
WoowBot pro comes with the possibility of creating mobile apps! Run WoowBot from your website as a native IOS or Android app! It requires minimum configurations and you can use either Ioninc or Phonegap platform of your choice to publish the apps! WoowBot Mobile app allows viewing product details, adding products to the cart and checkout through the APP using your website’s woocommerce pages.

Detailed documentations, instructions and Mobile APP source materials are available for both PhoneGap and Ionic when you buy WoowBot.

The mobile apps are ready to publish on Google Play Store or Apple’s ItunesConnect.


> [View Demo of WoowBot PRO Version](http://dev.quantumcloud.com/woowbot/)
> <strong>Upgrade to [WoowBot PRO](https://www.quantumcloud.com/products/woocommerce-chatbot-woowbot/)</strong> 


= Support, Bug Fix, Feature Request =

* We welcome your feedback and new feature requests for this WooCommerce chat bot! *
Let us know if you face any problem or need help with WooBot woocommerce chatbot.

* [Plugin Support Page](https://www.quantumcloud.com/products/support/) *


== Installation ==

1. Download the plugin zip file. Extract and upload in your wp-content/plugins folder.
2. From the wp-admin panel go to plugins and activate "WooWBot WooCommerce Chatbot"
4. You are done.


== Use ==

1. Simply Activating the Plugin will start showing WooWBot in the front end
2. Go to WooWBot under WooCommerce menu to adjust settings

== Frequently Asked Questions ==
= How can I upgrade from free version of WoowBot to the Pro version? =
1. Download the latest pro version of the plugin from our website
2. Log in to your WordPress admin area and go to the Plugins management page.
3. Deactivate and Delete the old version of the plugin
4. Upload and Activate the latest pro version of the plugin
5. You are done.

== Screenshots ==
1. Woocommerce chatbot hard at work
2. Woocommerce chatbot hard at work
3. Woocommerce chatbot HUD
4. Woocommerce chatbot HUD for Icon
5. Woocommerce chatbot HUD for Language Management


== Changelog ==
= 1.9.0 =
# Added option to Disable WoowBot to Load on Mobile Device

= 1.8.0 =
# Custom Icon Style issue fixed.
# Product status in Search result fixed.

= 1.7.0 =
# Making sure all previous settings and langagues stay after update.
# Mobile view improved.

= 1.6.0 =
# Product properties should not be accessed directly issue in log file fixed.

= 1.5.0 =
# Control panel UI Polishing
# Moved to Own Submenu
# Upgrade to Pro notice

= 1.4.0 =
# Multilingual feature enabled with mo/pot file
# Pro Version upgrade notification added
# Moved WoowBot to main menu

= 1.3.0 =
# In Mobile WoowBot Icon Position will change according to the settings
# Categories duplication issue fixed 
# Some langagues updated.

= 1.2.0 =
Fixed some search bug for mobile browsers

= 1.1.0 =
Fixed bug on window resize
Default options related issue fixed. version:1.1.0

= 1.0.0 =
Updated icons
Fixed some PHP and JS issues

= 0.9.0 =
*Inception

 == Upgrade Notice ==
= 1.0.0 =
Updated icons
Fixed some PHP and JS issues